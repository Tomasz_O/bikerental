package controller.bike;

import model.Bike;

import java.util.List;

public interface BikeController {

    List<Bike> getAllBikes();
    List<Bike> findByColour(String colour);
    List<Bike> findByBrand(String brand);
    List<Bike> findAvailableBikes();
    List<Bike> findByCost(double cost);
    Bike getBikeById(int Id);

}
