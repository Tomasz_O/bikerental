package controller.bike;

import model.Bike;

import java.util.ArrayList;
import java.util.List;

public class BikeControllerImpl implements BikeController {

    private List<Bike> listOfAllBikes;

    public BikeControllerImpl() {
        this.listOfAllBikes = new ArrayList<>();
        initBikes();
    }

    private void initBikes() {
        listOfAllBikes.add(new Bike(1, "Merida", "Ninety", "Red", "MTB", 45.0, false));
        listOfAllBikes.add(new Bike(2, "Giant", "Transe", "Blue", "MTB", 35.0, false));
        listOfAllBikes.add(new Bike(3, "Scott", "Contessa", "White", "Trekking", 25.0, false));
        listOfAllBikes.add(new Bike(4, "Kross", "Poland", "Black", "Cross", 20.0, true));
        listOfAllBikes.add(new Bike(5, "Cube", "Attention", "Red", "Road", 10.0, true));
        listOfAllBikes.add(new Bike(6, "Radon", "Winner", "Green", "MTB", 55.0, false));
        listOfAllBikes.add(new Bike(7, "Merida", "E-Bike", "Red", "MTB", 98.0, false));
        listOfAllBikes.add(new Bike(8, "Scott", "Road", "Red", "Road", 55.0, false));
        listOfAllBikes.add(new Bike(9, "Giant", "Kamciak", "Red", "Kids", 35.0, false));
        listOfAllBikes.add(new Bike(10, "Giant", "Norek", "Brown", "Woman", 35.0, false));
        listOfAllBikes.add(new Bike(11, "Giant", "Kuba", "Brown", "Woman", 35.0, true));
    }

    @Override
    public List<Bike> getAllBikes() {
        return listOfAllBikes;
    }

    @Override
    public List<Bike> findByColour(String colour) {
        List<Bike> bikesHaveUserChoiceColour = new ArrayList<>();
        for (Bike bike : listOfAllBikes) {
            if (bike.getColour().equals(colour)) {
                bikesHaveUserChoiceColour.add(bike);
            }
        }
        return bikesHaveUserChoiceColour;
    }

    @Override
    public List<Bike> findByBrand(String brand) {
        List<Bike> bikesHaveUserChoiceBrand = new ArrayList<>();
        for (Bike bike : listOfAllBikes) {
            if (bike.getBrand().equals(brand)) {
                bikesHaveUserChoiceBrand.add(bike);
            }
        }
        return bikesHaveUserChoiceBrand;
    }

    @Override
    public List<Bike> findAvailableBikes() {
        List<Bike> bikesIsRented = new ArrayList<>();
        for (Bike bike : listOfAllBikes) {
            if (!bike.isReserved()) {
                bikesIsRented.add(bike);
            }
        }
        return bikesIsRented;
    }

    @Override
    public List<Bike> findByCost(double cost) {
        List<Bike> bikesCostUnderUserCostPerHour = new ArrayList<>();
        for (Bike bike : listOfAllBikes) {
            if (bike.getCostBikeRentPerHour() <= cost) {
                bikesCostUnderUserCostPerHour.add(bike);
            }
        }
        return bikesCostUnderUserCostPerHour;
    }

    @Override
    public Bike getBikeById(int Id) {
        for (Bike bike : listOfAllBikes) {
            if (bike.getId() == Id) {
                return bike;
            }
        }
        return null;
    }

}
