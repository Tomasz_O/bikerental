package controller.rental;

import controller.bike.BikeController;
import controller.client.ClientController;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RentalControllerImpl implements RentalController {

    private BikeController bikeController;
    private ClientController clientController;
    private Map<Integer, Integer> reservationList = new HashMap<>();
    private Scanner scanner;


    public RentalControllerImpl(BikeController bikeController, ClientController clientController) {
        this.bikeController = bikeController;
        this.clientController = clientController;
    }

    @Override
    public Integer getReservedUserBikeId(int clientId) {
        return reservationList.get(clientId);
    }

    @Override
    public boolean isReservationListEmpty() {
        return reservationList.isEmpty();
    }

    @Override
    public boolean reserveBike(int clientId, int bikeId) {
        if (bikeController.getBikeById(bikeId).isReserved()) {
            return false;
        }
        bikeController.getBikeById(bikeId).setReserved(true);
        reservationList.put(clientId, bikeId);
        clientController.startReservationTime();
        return true;
    }

    @Override
    public double returnBike(int clientId) {
        scanner = new Scanner(System.in);
        clientController.endReservationTime();
        int reservedBikeId = reservationList.get(clientId);
        double currentClientBalance = clientController.charge(calculateReservationCost(reservedBikeId, clientController.getDuration()));
        while (currentClientBalance < 0) {
            System.out.println("No enough money! (" + currentClientBalance + ") Please top up accont! ");
            System.out.println("Please write how money top up.");
            double userWallet = scanner.nextDouble();
            currentClientBalance = clientController.toPay(userWallet);

        }
        bikeController.getBikeById(reservedBikeId).setReserved(false);
        return currentClientBalance;
    }

    private double calculateReservationCost(int bikeId, long duration) {
        System.out.println("Duration: " + duration / 10000.0 + " cost per hour: " + bikeController.getBikeById(bikeId).getCostBikeRentPerHour() + " = " + duration / 10000.0 * bikeController.getBikeById(bikeId).getCostBikeRentPerHour());
        return (double) duration / 10000.0 * bikeController.getBikeById(bikeId).getCostBikeRentPerHour();
    }
}
