package controller.rental;

public interface RentalController {

    boolean isReservationListEmpty();

    Integer getReservedUserBikeId(int clientId);

    boolean reserveBike(int clientId, int bikeId);

    double returnBike(int clientId);

}