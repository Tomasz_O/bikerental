package controller.client;

import model.Client;

public interface ClientController {

    double charge(double amount);

    double toPay(double amount);

    void setClient(Client client);

    double getWalletCurrentBalance();

    int getCurrentClientId();

    void startReservationTime();

    void endReservationTime();

    long getDuration();

}
