package controller.client;

import model.Client;

public class ClientControllerImpl implements ClientController {

    private Client client;

    @Override
    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public double getWalletCurrentBalance() {
        return client.getWallet().getCurrentBalance();
    }

    @Override
    public double charge(double amount) {
        client.setWallet(-amount);
        return client.getWallet().getCurrentBalance();
    }

    @Override
    public double toPay(double amount) {
        client.setWallet(amount);
        return client.getWallet().getCurrentBalance();
    }

    @Override
    public int getCurrentClientId() {
        return client.getId();
    }

    @Override
    public void startReservationTime() {
        client.setStartTime(System.currentTimeMillis());
        //System.out.println("Start: " + client.getStartTime());
    }

    @Override
    public long getDuration() {
        return client.getEndTime() - client.getStartTime();
    }

    @Override
    public void endReservationTime() {
        client.setEndTime(System.currentTimeMillis());
        //System.out.println("End: " + client.getEndTime());
    }

}
