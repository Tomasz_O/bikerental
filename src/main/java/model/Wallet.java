package model;

public class Wallet {

    private double currentBalance;

    public Wallet() {
        this.currentBalance = 0.0D;
    }

    public Wallet(double currentBalance) {
        this.currentBalance = currentBalance;
    }


    public void topUp(double amount){
        this.currentBalance +=amount;
    }
    public void charge (double value) {
        System.out.println("Current value: " + value);
        System.out.println("Current balance: "+ currentBalance);
        this.currentBalance -= value;
    }

    public void deposit(double value) {
        this.currentBalance  += value;
    }

    public double getCurrentBalance(){
        return this.currentBalance;
    }


}
