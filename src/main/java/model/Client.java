package model;

import java.time.LocalDateTime;

public class Client {
    private int id;
    private static int numberOfClient = 0;
    private String name;
    private String sureName;
    private String phoneNumber;
    private String emailAddress;
    private Wallet wallet = new Wallet();
    private long startTime;
    private long endTime;

    public Client() {
        this.id = numberOfClient++;
    }

    public Client(int id, String name, String sureName, String phoneNumber, String emailAddress, double money) {
        this.id = numberOfClient++;
        this.name = name;
        this.sureName = sureName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.wallet = new Wallet(money);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(double amount) {
        this.wallet.topUp(amount);
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}