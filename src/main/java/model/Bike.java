package model;


/**
 * Klasa na temat rowerow i atrybutow jakie maja je reprezentowac
 */

public class Bike {
    private int id;
    private String brand;
    private String model;
    private String colour;
    private String description;
    private double costBikeRentPerHour;
    private boolean reserved;

    public Bike(int IDBase, String brand, String model, String colour, String description, double costBikeRentPerHour, boolean reserved) {
        this.id = IDBase;
        this.brand = brand;
        this.model = model;
        this.colour = colour;
        this.description = description;
        this.costBikeRentPerHour = costBikeRentPerHour;
        this.reserved = reserved;
    }

    public boolean isReserved() {
        return reserved;
    }

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getColour() {
        return colour;
    }

    public double getCostBikeRentPerHour() {
        return costBikeRentPerHour;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    @Override
    public String toString() {
        return "Bike: " +
                "id: " + id +
                " | brand: '" + brand + '\'' +
                " | model: '" + model + '\'' +
                " | colour: '" + colour + '\'' +
                " | description: '" + description + '\'' +
                " | costBikeRentPerHour: " + costBikeRentPerHour +
                " |";
    }
}
