package view;


import controller.bike.BikeController;
import controller.bike.BikeControllerImpl;
import controller.client.ClientController;
import controller.client.ClientControllerImpl;
import controller.rental.RentalController;
import controller.rental.RentalControllerImpl;
import model.Bike;
import model.Client;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 */
public class ViewImpl implements View {

    private BikeController bikeController = new BikeControllerImpl();
    private ClientController clientController = new ClientControllerImpl();
    private RentalController rentalController = new RentalControllerImpl(bikeController, clientController);
    private Client client;
    private Scanner scanner;

    public ViewImpl() {
        run();
    }

    @Override
    public void run() {
        showTop();
        newClient();
        chooseRentalAction(client);
    }

    private void newClient() {
        System.out.println("Before you rent a bike we need some information from you!");
        scanner = new Scanner(System.in);
        Client client = new Client();

        System.out.println("Please write your name");
        String userName = scanner.nextLine();
        client.setName(userName);

        System.out.println("Please write your surname");
        String userSureName = scanner.nextLine();
        client.setSureName(userSureName);

        System.out.println("Please write your phone number");
        String userPhoneNumber = scanner.nextLine();
        client.setPhoneNumber(userPhoneNumber);

        System.out.println("Please write your email adress");
        String userEmailAdres = scanner.nextLine();
        client.setEmailAddress(userEmailAdres);

        client.setWallet(checkedMoneyCorrectFormat());

        clientController.setClient(client);
    }

    private void chooseRentalAction(Client client) {

        boolean isChosenFinished = false;
        while (!isChosenFinished) {
            showMenuOfRentalAction();
            scanner = new Scanner(System.in);
            int userChoice = checkedCorrectedFormat();

            switch (userChoice) {
                case 1:
                    printListOfBikes(bikeController.getAllBikes());
                    makeChoice();
                    break;
                case 2:
                    String colour = choseColour();
                    bikeController.findByColour(colour);
                    if (bikeController.findByColour(colour).size() == 0) {
                        System.out.println("There are no available bikes in color: " + colour);
                    } else {
                        printListOfBikes(bikeController.findByColour(colour));
                    }
                    makeChoice();
                    break;
                case 3:
                    String brand = choseBrand();
                    bikeController.findByBrand(brand);
                    if (bikeController.findByBrand(brand).size() == 0) {
                        System.out.println("There are no available bikes by brand: " + brand);
                    } else {
                        printListOfBikes(bikeController.findByBrand(brand));
                    }
                    makeChoice();
                    break;
                case 4:
                    printListOfBikes(bikeController.findAvailableBikes());
                    makeChoice();
                    break;
                case 5:
                    System.out.println("Choose the highest cost per hour that you accepted. Format 0,00");
                    double chosenCostPerHour = scanner.nextDouble();
                    if (bikeController.findByCost(chosenCostPerHour).size() == 0) {
                        System.out.println("We don't have bikes declared cost");
                    } else {
                        printListOfBikes(bikeController.findByCost(chosenCostPerHour));
                    }
                    makeChoice();
                    break;
                case 6:
                    if (rentalController.getReservedUserBikeId(clientController.getCurrentClientId()) == null) {
                        System.out.println("You haven't rented bike ;)");
                    } else {
                        int reservedBike = rentalController.getReservedUserBikeId(clientController.getCurrentClientId());
                        System.out.println(bikeController.getBikeById(reservedBike));
                        int returnChoice = makeReturnChoice();
                        if (returnChoice == reservedBike) {
                            double currentBalance = rentalController.returnBike(clientController.getCurrentClientId());
                            System.out.println("Your account balance is: " + currentBalance);
                            isChosenFinished = true;
                        }
                    }
                    break;
                case 7:
                    System.out.println("Your current balance is: " + clientController.getWalletCurrentBalance());
                    System.out.println();
                    break;
                case 8:
                    clientController.toPay(checkedMoneyCorrectFormat());
                    break;
                case 0:
                    client = null;
                    if (rentalController.isReservationListEmpty()) {
                        isChosenFinished = true;
                    } else {
                        System.out.println("Please end your reservation before you leave.");
                    }
                    break;
                default:
                    System.out.println("Wrong choice, please try again!");
            }
        }
    }

    private void printListOfBikes(List<Bike> listOfBike) {
        for (Bike bike : listOfBike) {
            System.out.println(bike);
        }
    }

    private int checkedCorrectedFormat() {
        boolean isFormatChoice = false;
        int userChoice = -1;
        while (!isFormatChoice) {
            System.out.println("Please write your choice");
            try {
                userChoice = scanner.nextInt();
                isFormatChoice = true;
            } catch (InputMismatchException e) {
                System.out.println("Wrong format, please try again");
                scanner.nextLine();
            }
        }
        return userChoice;
    }


    private double checkedMoneyCorrectFormat() {
        boolean isFormatChoice = false;
        double userWallet = 0;
        while (!isFormatChoice) {
            System.out.println("How many money do you pay to your wallet?");
            try {
                userWallet = scanner.nextDouble();
                isFormatChoice = true;
            } catch (InputMismatchException e) {
                System.out.println("You have entered the amount in wrong format. Correct format: 0,00 ");
                scanner.nextLine();
            }
        }
        return userWallet;
    }


    private boolean chosenBike() {
        scanner = new Scanner(System.in);
        boolean reservationSuceed = false;
        while (!reservationSuceed) {
            System.out.println("Please choose which bike do you want rent?");
            System.out.println("Bike id - To reserve ");
            System.out.println("0 - to exit");
            int chosenId = checkedCorrectedFormat();
            if (chosenId == 0) {
                return false;
            } else {
                reservationSuceed = rentalController.reserveBike(clientController.getCurrentClientId(), chosenId);
                if (!reservationSuceed) {
                    System.out.println("This bike is rented, please try again!");
                }
            }
        }
        return true;
    }

    private void makeChoice() {
        scanner = new Scanner(System.in);
        boolean isCorrectFormat = false;
        System.out.println("Do you want choose bike from above list?");
        System.out.println("1 - yes, I want choose bike" +
                "\n2 - no, I want make another choice.");
        while (true) {
            int choice = 0;
            try {
                choice = scanner.nextInt();
                isCorrectFormat = true;
            } catch (InputMismatchException e) {
                System.out.println("Wrong format, please try again");
                scanner.nextLine();
            }
            if (isCorrectFormat) {
                switch (choice) {
                    case 1:
                        chosenBike();
                        return;
                    case 2:
                        return;
                    default:
                        System.out.println("Wrong choice, please try again!");
                }
            }
        }
    }

    private int makeReturnChoice() {
        scanner = new Scanner(System.in);
        boolean isCorrectFormat = false;
        int choice = 0;
        while (!isCorrectFormat) {
            try {
                System.out.println("Select id to return bike, to exit choose 0");
                choice = scanner.nextInt();
                isCorrectFormat = true;
            } catch (InputMismatchException e) {
                System.out.println("Wrong format, please try again");
                scanner.nextLine();
            }
        }
        return choice;
    }

    private String choseColour() {
        scanner = new Scanner(System.in);
        String chosenColour = null;
        boolean isCorrectChoice = false;

        while (!isCorrectChoice) {
            System.out.println("Choose bike colour.");
            System.out.println("1 - Red" +
                    "\n2 - Blue" +
                    "\n3 - White" +
                    "\n4 - Black" +
                    "\n5 - Green" +
                    "\n6 - Brown");
            int userChoice = checkedCorrectedFormat();
            switch (userChoice) {
                case 1: {
                    chosenColour = "Red";
                    isCorrectChoice = true;
                    break;
                }
                case 2: {
                    chosenColour = "Blue";
                    isCorrectChoice = true;
                    break;
                }
                case 3: {
                    chosenColour = "White";
                    isCorrectChoice = true;
                    break;
                }
                case 4: {
                    chosenColour = "Black";
                    isCorrectChoice = true;
                    break;
                }
                case 5: {
                    chosenColour = "Green";
                    isCorrectChoice = true;
                    break;
                }
                case 6: {
                    chosenColour = "Brown";
                    isCorrectChoice = true;
                    break;
                }
                default:
                    System.out.println("You choose wrong, please choose right colour.");
            }
        }
        return chosenColour;
    }

    private String choseBrand() {
        scanner = new Scanner(System.in);
        String chosenBrand = null;
        boolean isCorrectChoice = false;

        while (!isCorrectChoice) {
            System.out.println("Choose bike's brand.");
            System.out.println("1 - Merida" +
                    "\n2 - Giant" +
                    "\n3 - Scott" +
                    "\n4 - Kross" +
                    "\n5 - Cube" +
                    "\n6 - Radon");
            int userChoice = checkedCorrectedFormat();
            switch (userChoice) {
                case 1: {
                    chosenBrand = "Merida";
                    isCorrectChoice = true;
                    break;
                }
                case 2: {
                    chosenBrand = "Giant";
                    isCorrectChoice = true;
                    break;
                }
                case 3: {
                    chosenBrand = "Scott";
                    isCorrectChoice = true;
                    break;
                }
                case 4: {
                    chosenBrand = "Kross";
                    isCorrectChoice = true;
                    break;
                }
                case 5: {
                    chosenBrand = "Cube";
                    isCorrectChoice = true;
                    break;
                }
                case 6: {
                    chosenBrand = "Radon";
                    isCorrectChoice = true;
                    break;
                }
                default:
                    System.out.println("You choose wrong, please choose right brand.");
            }
        }
        return chosenBrand;
    }


    private void showMenuOfRentalAction() {
        System.out.println("What do you want do?");
        System.out.println("1 - Show all bikes for rent.");
        System.out.println("2 - Show bike's by colour.");
        System.out.println("3 - Show bike's by brand.");
        System.out.println("4 - Show available bikes.");
        System.out.println("5 - Show bike's by cost per hour.");
        System.out.println("6 - Show my reservation and return bike.");
        System.out.println("7 - Show my wallet.");
        System.out.println("8 - Add money to wallet.");
        System.out.println("0 - Exit");

    }

    private void showTop() {
        System.out.println("*****************************************");
        System.out.println("******** B I K E   R E N T A L **********");
        System.out.println("*****************************************\n");
        System.out.println("===== Hello friend! What can I do for You? ===============");
    }

}
