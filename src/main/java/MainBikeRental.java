import view.ViewImpl;

/**
 * Main Class to run program.
 */
public class MainBikeRental {

    public static void main(String[] args) {
        new ViewImpl();
    }
}